""" 
This tools helps refurbed suppliers managing their offers in bulk by using CSV files as source of truth.  
"""

import sys
import os
import csv
from typing import Optional
from types import SimpleNamespace
from dataclasses import dataclass, asdict, fields
from dataclass_csv import DataclassReader
from rich.console import Console
import questionary
import pyfiglet
import requests
from chardet import detect

console = Console(color_system="auto")

custom_style_fancy = questionary.Style(
    [
        ("text", "fg:white"),
        ("question", "fg:red italic bold"),
        ("highlighted", "fg:yellow bold"),
        ("instruction", "fg:#808080"),
    ]
)


@dataclass
class Offer:
    """Class representing a refurbed offer"""

    sku: str
    instance_id: Optional[str] = None
    reference_min_price: Optional[str] = None
    grading: str = "A"
    warranty: str = "M12"
    stock: int = 0
    shipping_profile_id: str = "1"
    taxation: str = "GROSS"
    reference_currency_code: str = "EUR"
    reference_price: str = "9999"
    delete_reference_min_price: Optional[bool] = False


@dataclass
class Identifier:
    """Class for refurbed's external identifiers"""
    gtin: str


@dataclass
class MarketOffer:
    """Class representing a refurbed market offer"""

    sku: str
    market_code: str
    currency_code: str = "EUR"
    price: str = "9999"
    min_price: Optional[str] = None
    delete_min_price: Optional[bool] = False


class NestedNamespace(SimpleNamespace):
    """Helper class to be able to use dot notation with json responses"""
    def __init__(self, dictionary, **kwargs):
        super().__init__(**kwargs)
        for key, value in dictionary.items():
            if isinstance(value, dict):
                self.__setattr__(key, NestedNamespace(value))
            else:
                self.__setattr__(key, value)


def get_me():
    """API call to verify account incl. error handling"""
    headers = {
        "Authorization": f'Plain {os.environ["MERCH_TOKEN"]}',
        'User-Agent': 'refurbed-offers',
        'Content-Type': 'application/json'
    }
    try:
        response = requests.get("https://api.refurbed.io/v1/users/id/me/merchant/", timeout=10, headers=headers)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as errh:
        print(errh)
    except requests.exceptions.ConnectionError as errc:
        print(errc)
        sys.exit()
    except requests.exceptions.Timeout as errt:
        print(errt)
        sys.exit()
    except requests.exceptions.RequestException as err:
        print(err)
    return None


def set_token():
    """Sets entered merchant API token to env vars for this session"""
    console.print(
        "\n❗ THE ENTERED TOKEN WILL ONLY BE ACTIVE WITHIN THIS TERMINAL SESSION.\n\
❗ SETTING A TOKEN WILL OVERWRITE A PREVIOUSLY SET TOKEN.\n"
    )
    token = questionary.password(
        "Please enter your token",
        instruction="(Ctrl + C to abort)",
        style=custom_style_fancy,
    ).ask()
    if token is not None:
        os.environ["MERCH_TOKEN"] = str(token)
        if me_response := get_me():
            console.print(f'\nAccount: [magenta]{me_response["name"]}[/magenta] ({me_response["id"]})', justify="center")


def select_file():
    """Shows all CSV files in directory and let user select it"""
    # dir_path = Path(os.path.dirname(os.path.realpath(__file__)))
    # csv_files_in_dir = [file for file in os.listdir(dir_path.absolute()) if file.endswith('.csv')]
    csv_files_in_dir = [file for file in os.listdir() if file.endswith(".csv")]
    if not csv_files_in_dir:
        console.print(
            "[red]FAILED:[/] No csv file found in the script's directory. Please put the csv file in the same directory."
        )
        sys.exit()
    file = questionary.select(
        "Please select file",
        choices=csv_files_in_dir,
        instruction="(Use arrow keys, Ctrl + C to abort)",
        style=custom_style_fancy,
    ).ask()
    return file if file is not None else start_menu()


def determine_encoding(path):
    """Tries to determine file encoding and returns it to be used to open the file correctly or exits with error message"""
    with open(path, "rb") as file:
        charset = detect(file.read())
        if charset["encoding"] is None:
            console.print(
                f"Couldn't detect file encoding! Please make sure it's UTF-8. Debug info: {charset}"
            )
            sys.exit()
        console.print(f"File encoding: {charset}")
    return charset["encoding"].lower()


def header_validation(path, headers_to_check, charset):
    """Checks if file uses semicolon as delimiter and if all required headers exist in the CSV file"""
    errors_found = False
    with open(path, encoding=charset, newline='') as file:
        file_headers = csv.reader(file, delimiter=';')
        file_headers_list = next(file_headers)

        for header in headers_to_check:
            if header not in file_headers_list:
                errors_found = True
                console.print(
                    f"[red]ERROR:[/] Header '{header}' is missing, misspelled or upper case! Please also make sure to use ; as delimiter in your CSV."
                )
        if errors_found:
            sys.exit()


def read_file_into_dataclass(path, offer_dataclass, charset):
    """yields all the offer instances from the CSV file"""
    with open(path, encoding=charset) as file:
        try:
            yield from DataclassReader(file, offer_dataclass, delimiter=";")
        except KeyError as key_error:
            console.print(f"[red]ERROR:[/] {key_error}")
            sys.exit()


def post(url, data=None):
    """API call to refurbed merchant API incl. error handling"""
    headers = {
        "Authorization": f'Plain {os.environ["MERCH_TOKEN"]}',
        'User-Agent': 'refurbed-offers'
    }
    try:
        response = requests.post(url, json=data, timeout=10, headers=headers)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as errh:
        print(errh)
    except requests.exceptions.ConnectionError as errc:
        print(errc)
        sys.exit()
    except requests.exceptions.Timeout as errt:
        print(errt)
        sys.exit()
    except requests.exceptions.RequestException as err:
        print(err)
    return None


def append_matched_gtin_to_csv(path, data, charset):
    """Writes the matching result into the input CSV file in two new columns"""
    with open(path, 'r', encoding=charset) as file:
        reader = csv.reader(file, delimiter=';')
        existing_data = list(reader)

    if len(existing_data) != len(data):
        raise ValueError("Data length mismatch with existing CSV columns.")

    for i, row in enumerate(data):
        existing_data[i].extend(row)

    with open(path, 'w', newline='', encoding="utf-8") as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(existing_data)


def match_response_output(payload, response, matched_list):
    """Creates a strctured output list from BatchGetInstances response, also includes the GTIN"""
    for j, k in enumerate(response['results']):
        k = NestedNamespace(k)
        if k.status.code == 0:
            matched_list.append([k.instance.id, k.instance.name])
            console.print(f"{payload['instances'][j]['identifier']['gtin']} -> {k.instance.id}, {k.instance.name}")
        elif k.status.code == 5:
            matched_list.append(['none', k.status.message])
            console.print(f"{payload['instances'][j]['identifier']['gtin']} -> {k.status.message}")
    return matched_list


def create_response_output(payload, response):
    """Creates the specific terminal output of offer create for the user to check"""
    for j, k in enumerate(response["results"]):
        if k["status"]["message"] == "offer already exists":
            console.print(
                f"[red]FAILED:[/] SKU {payload['offers'][j]['sku']} -> {k['status']['message']}"
            )

        elif k["status"]["message"] == "offer created":
            log = [
                k["offer"]["id"],
                k["offer"]["instance_id"],
                k["offer"]["sku"],
                k["offer"]["grading"],
                k["offer"]["warranty"],
                str(k["offer"]["stock"]),
                str(k["offer"]["shipping_profile_id"]),
                k["offer"]["taxation"],
                k["offer"]["reference_price"],
                k["offer"]["instance_name"],
            ]
            if "reference_min_price" in k["offer"]:
                log.append(k["offer"]["reference_min_price"])
            console.print(f'[green]CREATED: [/]{"; ".join(log)}')

        else:
            console.print(k)


def update_response_output(payload, response):
    """Creates the specific terminal output of offer update for the user to check"""
    for j, k in enumerate(response["results"]):
        if k["status"]["message"] == "offer not found":
            console.print(
                f"[red]FAILED:[/] SKU [yellow]{payload['offers'][j]['identifier']['sku']}[/] -> {k['status']['message']}"
            )

        elif k["status"]["message"] == "offer updated":
            log = [
                k["offer"]["id"],
                k["offer"]["instance_id"],
                k["offer"]["sku"],
                k["offer"]["grading"],
                k["offer"]["warranty"],
                str(k["offer"]["stock"]),
                str(k["offer"]["shipping_profile_id"]),
                k["offer"]["taxation"],
                k["offer"]["reference_price"],
                k["offer"]["instance_name"],
            ]
            if "reference_min_price" in k["offer"]:
                log.append(k["offer"]["reference_min_price"])
            console.print(f'[green]UPDATED: [/]{"; ".join(log)}')

        else:
            console.print(k)


def create_market_response_output(payload, response):
    """Creates the specific terminal output of market offer create for the user to check"""
    for j, k in enumerate(response["results"]):
        if k["status"]["message"] == "market offer already exists":
            console.print(
                f"[red]FAILED:[/] SKU [yellow]{payload['market_offers'][j]['identifier']['sku']}, {payload['market_offers'][j]['market_price']['market_code']} -> {k['status']['message']}"
            )

        elif k["status"]["message"] == "market offer created":
            log = [
                k["market_offer"]["sku"],
                k["market_offer"]["market_price"]["market_name"],
                k["market_offer"]["market_price"]["currency_code"],
                k["market_offer"]["market_price"]["price"]
            ]
            if "min_price" in k["market_offer"]["market_price"]:
                log.append(k["market_offer"]["market_price"]["min_price"])
            console.print(f'[green]CREATED: [/]{"; ".join(log)}')

        else:
            console.print(k)


def update_market_response_output(payload, response):
    """Creates the specific terminal output of market offer update for the user to check"""
    for j, k in enumerate(response["results"]):
        if k["status"]["message"] == "market offer not found":
            console.print(
                f"[red]FAILED:[/] SKU [yellow]{payload['market_offers'][j]['identifier']['offer_id']['sku']}, {payload['market_offers'][j]['identifier']['market_code']}[/] -> {k['status']['message']}"
            )

        elif k["status"]["message"] == "market offer updated":
            log = [
                k["market_offer"]["sku"],
                k["market_offer"]["market_price"]["market_name"],
                k["market_offer"]["market_price"]["currency_code"],
                k["market_offer"]["market_price"]["price"]
            ]
            if "min_price" in k["market_offer"]["market_price"]:
                log.append(k["market_offer"]["market_price"]["min_price"])
            console.print(f'[green]UPDATED: [/]{"; ".join(log)}')

        else:
            console.print(k)


def chunk_offerlist_and_commit(offers_list):
    """Chunks the offerlist into batches of 50 and asks user to start the prod run"""
    chunked_offers_list = [
        offers_list[i : i + 50] for i in range(0, len(offers_list), 50)
    ]
    console.print(chunked_offers_list)

    commit = questionary.confirm(
        "Is everything ok? Do you want to commit?",
        default=False,
        auto_enter=False,
        style=custom_style_fancy,
    ).ask()
    print()
    return commit, chunked_offers_list


def match_gtin(host):
    """This function reads the GTIN/EAN from the column named gtin in the selected CSV and matches them against refurbed's database (BatchGetInstances)
    The matching result will be shown in the console directly and then the user can select to write the results directly into the initial CSV file, to move
    on with offer creation"""
    file = select_file()
    charset = determine_encoding(file)

    gtin_list = []
    for offer in read_file_into_dataclass(file, Identifier, charset):
        single_gtin = {"identifier": {"gtin": offer.gtin}}
        gtin_list.append(single_gtin)

    commit, chunked_gtin_list = chunk_offerlist_and_commit(gtin_list)

    if commit:
        if "MERCH_TOKEN" not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchGetInstances"
        matched_list = [['instance_id', 'product_name']]
        for i, _ in enumerate(chunked_gtin_list):
            payload = {"instances": chunked_gtin_list[i]}
            response = post(url, payload)
            if response is not None:
                matched_list = match_response_output(payload, response, matched_list)

        write_commit = questionary.confirm(
            "Do you write matching data to your file?",
            default=False,
            auto_enter=False,
            style=custom_style_fancy,
        ).ask()
        print()

        if write_commit:
            append_matched_gtin_to_csv(file, matched_list, charset)


def create_offers(host):
    """This function creates and chunks the payload for batch routes. A preview will be displayed to the user.
    If everything checks out user can commit and offers will be created according to the CSV
    """
    create_headers = [
        "sku",
        "instance_id",
        "reference_min_price",
        "grading",
        "warranty",
        "stock",
        "shipping_profile_id",
        "taxation",
        "reference_currency_code",
        "reference_price"
    ]

    file = select_file()
    charset = determine_encoding(file)
    header_validation(file, create_headers, charset)

    offers_list = [
        asdict(offer) for offer in read_file_into_dataclass(file, Offer, charset)
    ]
    for offer in offers_list:
        del offer["delete_reference_min_price"]
        if offer["reference_min_price"] == "0":
            offer["reference_min_price"] = None

    commit, chunked_offers_list = chunk_offerlist_and_commit(offers_list)

    if commit:
        if "MERCH_TOKEN" not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchCreateOffers"
        for i, _ in enumerate(chunked_offers_list):
            payload = {"offers": chunked_offers_list[i]}
            response = post(url, payload)
            if response is not None:
                create_response_output(payload, response)


def update_offers(host):
    """This function asks the user to select the to be updated fields then creates and chunks the payload for batch routes.
    A preview will be displayed to the user. If everything checks out user can commit and offers will be updated according to the CSV
    """
    choices = [field.name for field in fields(Offer)]
    choices.remove("instance_id")
    choices.remove("sku")
    choices.remove("delete_reference_min_price")

    selected_fields = questionary.checkbox(
        "Please select fields to update. Fields must exist in file, sku is mandatory)",
        choices=choices,
        style=custom_style_fancy,
    ).ask()
    if not selected_fields:
        start_menu()

    file = select_file()

    selected_fields.append("sku")
    charset = determine_encoding(file)
    header_validation(file, selected_fields, charset)
    selected_fields.remove("sku")

    selected_fields.append("delete_reference_min_price")

    offers_list = []
    for offer in read_file_into_dataclass(file, Offer, charset):
        single_offer = {"identifier": {"sku": offer.sku}}

        for field in selected_fields:
            if field == "reference_min_price" and offer.__dict__[field] == "0":
                offer.delete_reference_min_price = True
            else:
                single_offer[field] = offer.__dict__[field]

        if not single_offer["delete_reference_min_price"]:
            del single_offer["delete_reference_min_price"]
        offers_list.append(single_offer)

    commit, chunked_offers_list = chunk_offerlist_and_commit(offers_list)

    if commit:
        if "MERCH_TOKEN" not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchUpdateOffers"
        for i, _ in enumerate(chunked_offers_list):
            payload = {"offers": chunked_offers_list[i]}
            response = post(url, payload)
            if response is not None:
                update_response_output(payload, response)


def delete_offers(host):
    """This function creates and chunks the payload for batch routes. A preview will be displayed to the user.
    If everything checks out user can commit and offers will be deleted according to the CSV
    """
    file = select_file()
    charset = determine_encoding(file)
    header_validation(file, ["sku"], charset)

    offers_list = [
        {"identifier": {"sku": offer.sku}}
        for offer in read_file_into_dataclass(file, Offer, charset)
    ]

    commit, chunked_offers_list = chunk_offerlist_and_commit(offers_list)

    if commit:
        if "MERCH_TOKEN" not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchDeleteOffers"
        for i, _ in enumerate(chunked_offers_list):
            payload = {"offers": chunked_offers_list[i]}
            response = post(url, payload)
            if response is not None:
                console.print(response)


def create_market_offer_request_body(path):
    """This function prepares the create market offer request body"""
    charset = determine_encoding(path)
    market_offers_list = [asdict(market_offer) for market_offer in read_file_into_dataclass(path, MarketOffer, charset)]
    if market_offers_list is not None:
        return [
            {
                "identifier": {"sku": dic["sku"]},
                "market_price": {
                    "market_code": dic["market_code"],
                    "currency_code": dic["currency_code"],
                    "price": dic["price"],
                    "min_price": dic["min_price"],
                },
            }
            for dic in market_offers_list
        ]
    return None


def create_market_offers(host):
    """This function creates and chunks the payload for batch routes. A preview will be displayed to the user.
    If everything checks out user can commit and market offers will be created according to the CSV.
    """

    create_headers = [
        "sku", 
        "market_code",
        "min_price",
        "currency_code",
        "price"
    ]

    file = select_file()
    charset = determine_encoding(file)
    header_validation(file, create_headers, charset)

    prepare_payload = create_market_offer_request_body(file)

    if prepare_payload is not None:
        for market_offer in prepare_payload:
            if market_offer['market_price']['min_price'] == "0":
                market_offer['market_price']['min_price'] = None

    commit, chunked_market_offers_list = chunk_offerlist_and_commit(prepare_payload)

    if commit:
        if 'MERCH_TOKEN' not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchCreateMarketOffer"
        for i, _ in enumerate(chunked_market_offers_list):
            payload = {"market_offers": chunked_market_offers_list[i]}
            response = post(url, payload)
            if response is not None:
                create_market_response_output(payload, response)


def update_market_offers(host):
    """This function asks the user to select the to be updated fields then creates and chunks the payload for batch routes.
    A preview will be displayed to the user. If everything checks out user can commit and market offers will be updated according to the CSV
    """

    choices = [field.name for field in fields(MarketOffer)]
    choices.remove('sku')
    choices.remove('market_code')
    choices.remove('delete_min_price')

    selected_fields = questionary.checkbox(
        "Please select fields to update. Fields must exist in file, sku is mandatory)",
        choices=choices,
        style=custom_style_fancy,
    ).ask()
    if not selected_fields:
        start_menu()

    file = select_file()

    selected_fields.append('sku')
    selected_fields.append('market_code')
    charset = determine_encoding(file)
    header_validation(file, selected_fields, charset)
    selected_fields.remove('sku')
    selected_fields.remove('market_code')

    selected_fields.append('delete_min_price')

    market_offers_list = []
    for market_offer in read_file_into_dataclass(file, MarketOffer, charset):
        add_fields = {}

        for field in selected_fields:
            if field == "min_price" and market_offer.__dict__[field] == "0":
                market_offer.delete_min_price = True
            else:
                add_fields[field] = market_offer.__dict__[field]

        single_market_offer = {"identifier": {
                                    "offer_id": {
                                    "sku": market_offer.sku},
                                    "market_code": market_offer.market_code},
                                    "market_price": add_fields}

        if not single_market_offer["market_price"]["delete_min_price"]:
            del single_market_offer["market_price"]["delete_min_price"]

        market_offers_list.append(single_market_offer)

    commit, chunked_market_offers_list = chunk_offerlist_and_commit(market_offers_list)

    if commit:
        if 'MERCH_TOKEN' not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchUpdateMarketOffer"
        for i, _ in enumerate(chunked_market_offers_list):
            payload = {"market_offers": chunked_market_offers_list[i]}
            response = post(url, payload)
            if response is not None:
                update_market_response_output(payload, response)


def delete_market_offers(host):
    """This function creates and chunks the payload for batch routes. A preview will be displayed to the user.
    If everything checks out user can commit and market offers will be deleted according to the CSV
    """

    file = select_file()
    charset = determine_encoding(file)
    header_validation(file, ["sku", "market_code"], charset)

    market_offers_list = [{"identifier": {
                                "offer_id": {
                                    "sku": market_offer.sku
                                    },
                                "market_code": market_offer.market_code}}
                            for market_offer in read_file_into_dataclass(file, MarketOffer, charset)]

    commit, chunked_market_offers_list = chunk_offerlist_and_commit(market_offers_list)

    if commit:
        if 'MERCH_TOKEN' not in os.environ:
            console.print("❗ No API token found! You must enter a token first.")
            start_menu()

        url = f"{host}BatchDeleteMarketOffer"
        for i, _ in enumerate(chunked_market_offers_list):
            payload = {"offers": chunked_market_offers_list[i]}
            response = post(url, payload)
            if response is not None:
                console.print(response)


def menu():
    """Draws the main menu where user chooses action"""
    console.rule()
    actions = [
        "Enter your API token",
        "Match GTIN",
        "Create offers",
        "Update offers",
        "Delete offers",
        "Create market offers",
        "Update market offers",
        "Delete market offers",
        "Quit",
    ]
    return questionary.select(
        "Please select action", choices=actions, style=custom_style_fancy
    ).ask()


def selection(action):
    """Commences the users selection in main menu"""
    offer_host = "https://api.refurbed.com/refb.merchant.v1.OfferService/"
    marketoffer_host = "https://api.refurbed.com/refb.merchant.v1.MarketOfferService/"

    if action == "Match GTIN":
        match_host = "https://api.refurbed.com/refb.merchant.v1.InstanceService/"
        match_gtin(match_host)

    if action == "Enter your API token":
        set_token()

    if action == "Create offers":
        create_offers(offer_host)

    if action == "Update offers":
        update_offers(offer_host)

    if action == "Delete offers":
        delete_offers(offer_host)

    if action == "Create market offers":
        create_market_offers(marketoffer_host)

    if action == "Update market offers":
        update_market_offers(marketoffer_host)

    if action == "Delete market offers":
        delete_market_offers(marketoffer_host)

    if action == "Quit":
        sys.exit()


def start_menu():
    """Starts the interactive menu in the terminal"""
    while True:
        action = menu()
        selection(action)


def main() -> None:
    """Creates the ASCII Heading and runs the menu"""
    print("\033c")
    console.rule()
    console.print("v 2.0.0", justify="right")
    console.print(pyfiglet.figlet_format("refurbed* Offers", width=200))
    start_menu()


if __name__ == "__main__":
    main()
