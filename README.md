# Community Contribution Utilities

All community tools are to be used at your own risk. While we tested them to a certain degree, you are responsible for your own actions.

## refurbed-offers

refurbed-offers is a command line tool (CLI) to create, update and delete offers and market offers in batches from a CSV file. To use the tool you need to be an active seller on refurbed.

## Installation

Download [artifacts.zip](https://gitlab.com/refurbed-community/oss/community-contrib-utilities/-/jobs/artifacts/main/download?job=build_windows) and extract refurbed-offers.exe binary from the download section. You want to put it in a separate directory to make working with files easier.

## CSV file preparation for offer management

Create a CSV file with a semicolon (";") as separator and the following headers or download the example file from the repo. The headers must be named exactly as in the table below.

Not all columns are required for each operation. For `create` you need all columns, for `updates` only the one's that you want to update, for `delete` only `sku`.

For create and update there will be default values if you leave values empty in certain columns, please see the table below. You will also see these default values in the dry run before you commit to the operation.

| header name | description | default value if unset | type |
| --- | --- | --- | --- |
| instance_id | refurbed’s internal reference number, can be found in our product catalogue (download in merchant interface) |  | numeric |
| sku | SKU that will be connected to the offer, it will be the reference if you e.g. want to do offer updates via API. MUST be unique |  | string |
| reference_min_price | Can be updated via API or in merchant backend or you set an initial value | null | string |
| reference_price | Can be updated via API/CSV Export or in merchant backend or you set an initial value | 9999 | string |
| reference_currency_code | Offer currency as ISO 4217 code. 3 letter abbreviation e.g. SEK, EUR, DKK | EUR | string |
| stock | Can be updated via API/CSV Export or in merchant backend or you set an initial value | 0 | numeric |
| grading | A, B or C. Please refer to our Quality charta | A | string |
| warranty | Allowed values are M12. M = Months | M12 | string |
| shipping_profile_id | ShippingProfile ID can be found in the merchant interface > Shipping profiles section. The choosen id will be connected to the offer. |  | numeric |
| taxation | Allowed values are MARGINAL or GROSS | GROSS | string |

## CSV file preparation for market offer management

Create a CSV file with a semicolon (";") as separator and the following headers or download the example file from the repo. The headers must be named exactly as in the table below.

Not all columns are required for each operation. The column `sku` and `market_code` are always required. For `create` you need all columns, for `updates` only the one's that you want to update, for `delete` only `sku` and `market_code`.

For create and update there will be default values if you leave values empty in certain columns, please see the table below. You will also see these default values in the dry run before you commit to the operation.

| header name | description | default value if unset | type |
| --- | --- | --- | --- |
| sku | SKU that will be connected to the offer, it will be the reference if you e.g. want to do offer updates via API. MUST be unique |  | string |
| market_code | The code of the market where you want to set a specific market price. |  | string |
| min_price | Can be updated via API or in merchant backend or you set an initial value | null | string |
| price | Can be updated via API or in merchant backend or you set an initial value | 9999 | string |
| currency_code | Offer currency as ISO 4217 code. 3 letter abbreviation e.g. SEK, EUR, DKK | EUR | string |

## Usage

Open your console ([Windows Terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701) strongly recommended) and navigate to the folder where you downloaded the offers tool. 

It's recommended to maximize the console window, because with the following actions there will be a lot of data displayed in the dry runs. On a maximized window the formatter will use full width which makes checking way easier.

Run the offers tool with:

`.\refurbed-offers.exe` in PowerShell 

or

`refurbed-offers.exe` in cmd

The tool will present itself with an interactive menu. Navigate with Arrow up/down keys and Enter.

### Authentication

To use the tool you need to enter your prod API token. It will be stored in environment vars as long as the terminal session lasts. You can request your token at integrations@refurbed.com. 

Select `Enter your API token` in the menu. Copy + Paste your API token (without "Plain", make sure no leading/trailing whitespaces). Press Enter to store it for the session. You will be lead back to the main menu.

### Match GTIN

The tool assumes you prepared your data in a CSV file in correct format for your operation. The file must be in the same directory as the tool. Every line holds a GTIN-13. **Mandatory** is the `gtin` column. Other fields will be ignored.

In the menu choose `Match GTIN`, the tool will only show you files of type CSV within the directory. Choose the prepared file, the tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 items (Batch route limit). 

If you're happy you can commit by hitting `Y` and `Enter` (hitting only `Enter` will abort the action).

The tool will start matching your GTINs against refurbed's database. Every GTIN will return a response with either `instance_id` and `product_name` or `instance not found`.

Once the matching is done, the tool will ask you if you want to write the data directly to your provided CSV file. If you choose `Yes` it will append `instance_id` aswell as `product_name` in two separate columns of your input file. Any other columns e.g. SKU, price, grading should be left untouched.

If you notice any mismatching GTIN please reach out to refurbed.

### Create offers

The tool assumes you prepared your offer data in a CSV file in correct format for your operation. The file must be in the same directory as the tool. Every line represents a new offer. **Mandatory** are `instance_id`, `sku` and `shipping_profile_id`. All other fields will default if left empty.

In the menu choose `Create offers`, the tool will only show you files of type CSV within the directory. Choose the prepared file, the tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 item (Batch route limit). 

If you maximized the window every offer is one line. In case that your SKU is very long or Screen width below 1920px the formatter may resort to an indented view.

**CHECK** this data thoroughly. If you left some fields empty, that have a default value as described in the table above, you will see these now too in constructed payloads.

If you're happy you can commit by hitting `Y` and `Enter` (hitting only `Enter` will abort the action).

The tool will start creating the offers in your account. Although the tool uses refurbed's Batch routes it may take some time depending on the amount of offers to be created.

Every line/offer will return a response about the success or failure of the operation. Most common failure is that a SKU already exist in your account (they must be unique). If that is the case you will get a response like `SKU: <sku> -> Offer already exists`.

### Update offers

The tool assumes you prepared your update data in a CSV file in correct format for your operation. The file must be in the same directory as the tool. Every line represents an offer update. The file must contain the columns with the data you want to update (obviously). Reference is your SKU (the SKU that was set on offer creation).
`instance_id` is not needed and will be ignored. If you choose a field to update that has a header in the file but the fields for each offer are left empty, it will also resort to the default value.

In the menu choose `Update offers`, you will be presented with a multiselect list of all updateable offer fields. Select all fields you want to update with `Space`. All offer fields, that you want to update must have a corresponding column in the file. If you have a column in your file but no values for each offer and this field has a default value, it will take the default value (see table above)

`reference_min_price` has a special treatment as it can also be deleted completely. This is possible by setting `0`  in the CSV file fields in the `reference_min_price` column on every offer where you want to delete it. Note: if you leave the values in the column just empty, they will default to `null` and therefore no changes will be made.

Next the tool will prompt you to select a file of type CSV within the directory. Choose the prepared file, the tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 item (Batch route limit).

**CHECK** this data thoroughly. If you left some fields, that have a default value as described in the table above, empty, you will see them now too in constructed payloads.

If you're happy you can commit by hitting `Y` and `Enter` (hitting only `Enter` will abort the action).

The tool will start updating the offers in your account. Although the tool uses refurbed's Batch routes it may take some time depending on the amount of offers to be updated.

Every line/offer will return a response about the success or failure of the operation. Most common failure is that a SKU does not exist in your account (you cannot update what does not exist). If that is the case you will get a response like `SKU: <sku> -> Offer not found`.

### Delete offers

Batch offer deletion only needs a CSV file with one column named `sku` containing all to be deleted SKU's of your offers. Other columns will be ignored.

In the menu choose `Delete offers`, then select the CSV file for offer deletion. The tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 item (Batch route limit).

**CHECK** this data thoroughly. If everything is alright, commit with `Y` and `Enter`.

The tool will start deleting the offers from your account.

Be aware that this action cannot be reversed. It is always a good idea to trigger an Offer Export from your account before you delete, so that, in case of a mistake, you can batch recreate these offers.

### Create market offers

The tool assumes you prepared your market offer data in a CSV file in the correct format for your operation. The file must be in the same directory as the tool. Every line represents a new offer. **Mandatory** are `sku` and `market_code`. All other fields will default if left empty.

In the menu choose `Create market offers`, the tool will only show you files of type CSV within the directory. Choose the prepared file, the tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 item (Batch route limit). 

If you maximized the window every market offer is one line. In case that your SKU is very long or Screen width below 1920px the formatter may resort to an indented view.

**CHECK** this data thoroughly. If you left some fields empty, that have a default value as described in the table above, you will see these now too in constructed payloads.

If you're happy you can commit by hitting `Y` and `Enter` (hitting only `Enter` will abort the action).

The tool will start creating the market offers in your account. Although the tool uses refurbed's Batch routes it may take some time depending on the amount of market offers to be created.

Every line/market offer will return a response about the success or failure of the operation. Most common failure is that the market-specific offer already exist in your account (once the market offer is created in can be updated directly). If that is the case you will get a response like `SKU: <sku> -> market offer already exists`.

### Update market offers

The tool assumes you prepared your update data in a CSV file in correct format for your operation. The file must be in the same directory as the tool. Every line represents an market offer update. The file must contain the columns with the data you want to update (obviously). Reference is your SKU (the SKU that was set on offer creation) and the market code.
If you choose a field to update that has a header in the file but the fields for each market offer are left empty, it will also resort to the default value.

In the menu choose `Update market offers`, you will be presented with a multiselect list of all updateable market offer fields. Select all fields you want to update with `Space`. All market offer fields, that you want to update must have a corresponding column in the file. If you have a column in your file but no values for each market offer and this field has a default value, it will take the default value (see table above)

`min_price` has a special treatment as it can also be deleted completely. This is possible by setting `0` in the CSV file fields of the `min_price` column on every market offer where you want to delete it. Note: if you leave the values in the column just empty, they will default to `null` and therefore no changes will be made.

Next the tool will prompt you to select a file of type CSV within the directory. Choose the prepared file, the tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 item (Batch route limit).

**CHECK** this data thoroughly. If you left some fields, that have a default value as described in the table above, empty, you will see them now too in constructed payloads.

If you're happy you can commit by hitting `Y` and `Enter` (hitting only `Enter` will abort the action).

The tool will start updating the market offers in your account. Although the tool uses refurbed's Batch routes it may take some time depending on the amount of offers to be updated.

Every line/market offer will return a response about the success or failure of the operation. Most common failure is that the market offer was not created in your account (you cannot update what does not exist). If that is the case you will get a response like `SKU: <sku> -> market offer not found`.

### Delete market offers

Batch market offer deletion only needs a CSV file with the `sku` and the `market_code` containing all market-specific prices to be deleted. Other columns will be ignored.

In the menu choose `Delete market offers`, then select the CSV file for market offer deletion. The tool will instantly show you in a dry run all data from the file that it intends to send to the API as a json payload in batches of max. 50 item (Batch route limit).

**CHECK** this data thoroughly. If everything is alright, commit with `Y` and `Enter`.

The tool will start deleting the market-specific offers from your account.

Be aware that this action cannot be reversed. It is always a good idea to trigger an Offer Export from your account before you delete, so that, in case of a mistake, you can batch recreate these market offers.

## Examples

#### Authentication

![](https://user-images.githubusercontent.com/24986684/228772108-c55677f0-0a4f-4a2f-a83b-d67abf6b013d.mp4)


#### Offer create

![](https://user-images.githubusercontent.com/24986684/228772518-217ee89b-0f09-4056-9baa-57433f47e3e6.mp4)


#### Offer update

![](https://user-images.githubusercontent.com/24986684/228772557-8aa760f7-12f3-432a-827a-4677a14ce608.mp4)


#### Offer delete

![](https://user-images.githubusercontent.com/24986684/228772592-c49eb157-7f7d-413b-85ca-d08a2c65c1c3.mp4)

## Documentation

[refurbed API](https://refurbed.notion.site/refurbed-API-7be4e188412d4ea2bd35ef339dab9e3e)

## Authors

- LaTedesca
- [@AFROnin](https://gitlab.com/AFROnin)

## License

Copyright (c) 2023 refurbed GmbH

See LICENSE for details.
